package com.citm.chatsonya.android.controller;

import com.citm.chatsonya.android.dto.PostResponse;
import com.citm.chatsonya.android.dto.model.loginModel;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.helpers.AsyncView;
import com.citm.chatsonya.android.http.AllDataService;
import com.google.firebase.iid.FirebaseInstanceId;
import com.shipdream.lib.android.mvc.FragmentController;
import com.shipdream.lib.android.mvc.Reason;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginController extends FragmentController<loginModel, AsyncView> {
    AllDataService allDataService;
    @Override
    public Class<loginModel> modelType() {
        return loginModel.class;
    }

    @Override
    public void onViewReady(Reason reason) {
        super.onViewReady(reason);
//        view.hideLoadingStatus();
    }

    public void login(String user , String pass){
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
         allDataService = RetrofitInstance.RetrofitInstance().create(AllDataService.class);
        Call<PostResponse> call = allDataService.login(user,pass , refreshedToken);
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
             if (response.isSuccessful()){
                    view.loginsubmit(response.body().getResult().get(0).getStatus() ,response.body().getResult().get(0).getToken(),response.body().getResult().get(0).getId_user());
                }else{
                    view.loginsubmit("Gagal melakukan login" ,"kosong" , null );
                }
            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {

            }
        });


    }


}
