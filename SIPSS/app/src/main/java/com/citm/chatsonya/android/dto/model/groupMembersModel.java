package com.citm.chatsonya.android.dto.model;

public class groupMembersModel {
    String id_conversation , id;

    public groupMembersModel(String id_conversation, String id) {
        this.id_conversation = id_conversation;
        this.id = id;
    }

    public String getId_conversation() {
        return id_conversation;
    }

    public String getId() {
        return id;
    }
}
