package com.citm.chatsonya.android.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.dto.model.usersmodel;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.citm.chatsonya.android.view.chat.AddUser;
import com.shipdream.lib.android.mvc.NavigationManager;

import java.util.List;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.userViewHolder> {

    private List<usersmodel> datalist;
    @Inject
    NavigationManager navigationManager;
    AddUser context;
    DatabaseHandler db;

    public UserAdapter(List<usersmodel> datalist, NavigationManager navigationManager, AddUser context , DatabaseHandler db) {
        this.datalist = datalist;
        this.navigationManager = navigationManager;
        this.context = context;
        this.db = db;
    }

    @NonNull
    @Override
    public userViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_user,parent,false);
        return new userViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final userViewHolder holder, final int position) {
        holder.textemail.setText(datalist.get(position).getEmail());
        holder.textnama.setText(datalist.get(position).getName());

        holder.imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.linearLayout.setVisibility(View.GONE);
                context.kirimfriendlist(datalist.get(position).getId());
                db.AddUser(datalist.get(position));

            }
        });

    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class userViewHolder extends RecyclerView.ViewHolder {
        TextView textnama, textemail;
        CircleImageView circleImageView;
        LinearLayout linearLayout;
        ImageView imgview;
        CardView cv;

        public userViewHolder(View itemView) {
            super(itemView);
            textnama = (TextView) itemView.findViewById(R.id.nama_user);
            textemail = (TextView) itemView.findViewById(R.id.email_user);
            circleImageView = (CircleImageView)itemView.findViewById(R.id.gambar_berita);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.pindah_adduser);
            imgview = (ImageView)itemView.findViewById(R.id.tambah_user);
            cv = (CardView)itemView.findViewById(R.id.user_list);


        }

    }
}
