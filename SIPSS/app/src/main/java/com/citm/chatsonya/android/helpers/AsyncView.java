package com.citm.chatsonya.android.helpers;


import com.shipdream.lib.android.mvc.UiView;

public interface AsyncView extends UiView {
    void showLoadingStatus();
    void hideLoadingStatus();
    void loginsubmit(String status, String Key, String iduser);
}