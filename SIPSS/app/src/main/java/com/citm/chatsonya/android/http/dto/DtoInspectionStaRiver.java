

package com.citm.chatsonya.android.http.dto;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class DtoInspectionStaRiver extends DtoParent implements Serializable {

    @SerializedName("data")
    private ArrayList<DtoInspectionStaRiverData> data;
    public DtoInspectionStaRiver(){

    }
}
