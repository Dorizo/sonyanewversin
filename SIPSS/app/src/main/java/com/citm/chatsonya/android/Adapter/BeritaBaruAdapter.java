package com.citm.chatsonya.android.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.controller.DetailBeritaBaruController;
import com.citm.chatsonya.android.dto.model.BeritaBaruModel;
import com.google.gson.Gson;
import com.shipdream.lib.android.mvc.NavigationManager;
import com.shipdream.lib.android.mvc.Preparer;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;



public class BeritaBaruAdapter extends  RecyclerView.Adapter<BeritaBaruAdapter.EmployeeViewHolder>  {

    private List<BeritaBaruModel> datalist;
    @Inject
    NavigationManager navigationManager;
    Context context;
    public BeritaBaruAdapter(List<BeritaBaruModel> datalist , Activity context, NavigationManager nav ) {
        this.datalist = datalist;
        this.context = context;
        this.navigationManager = nav;
    }

    @NonNull
    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_beritabaru,parent,false);
        return new EmployeeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeViewHolder holder, final int position) {
        holder.txtEmpName.setText(datalist.get(position).getJudul_berita());
        holder.txtEmpEmail.setText(datalist.get(position).getCreated_at());
        Picasso.with(holder.itemView.getContext()).load(context.getResources().getString(R.string.url_image)+datalist.get(position).getGambar_berita()).into(holder.circleImageView);
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vx) {
                navigationManager.navigate(context).with(DetailBeritaBaruController.class, new Preparer<DetailBeritaBaruController>() {
                    @Override
                    public void prepare(DetailBeritaBaruController controller_to) {
                        Gson gs = new Gson();
                        String s = gs.toJson(datalist.get(position));
                        controller_to.x(s);
                    }
                }).to(DetailBeritaBaruController.class);
            }
        });
    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class EmployeeViewHolder extends RecyclerView.ViewHolder {
        TextView txtEmpName, txtEmpEmail;
        ImageView circleImageView;
        CardView linearLayout;
        public EmployeeViewHolder(View itemView) {
            super(itemView);
            txtEmpName = (TextView) itemView.findViewById(R.id.judul_beritabaru);
            txtEmpEmail = (TextView) itemView.findViewById(R.id.readmore_beritabaru);
            circleImageView = (ImageView)itemView.findViewById(R.id.gambar_berita);
            linearLayout = (CardView)itemView.findViewById(R.id.pindah);




        }
    }
}
