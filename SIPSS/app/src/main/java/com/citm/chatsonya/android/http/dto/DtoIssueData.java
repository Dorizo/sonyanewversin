

package com.citm.chatsonya.android.http.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class DtoIssueData implements Serializable {

    @SerializedName("id")
    public String id_online;
    public String id_local;
    public String user_id;
    public String balai_id;
    public String type_id;
    public String type_name;
    public String gps_lat;
    public String gps_lng;
    public String location;
    public String caption;
    public String category;
    public String status;
    public String fullname;
    public String user_photo;
    public String balai_name;
    public String published;
    public String user_added;
    public String user_modified;
    public String date_added;
    public String date_modified;
    @SerializedName("files_kerusakan")
    private ArrayList<DtoIssueFileData> files_kerusakan;
    @SerializedName("files_perbaikan")
    private ArrayList<DtoIssueFileData> files_perbaikan;
    @SerializedName("files_finishing")
    private ArrayList<DtoIssueFileData> files_finishing;
    public DtoIssueData(){

    }

}
