package com.citm.chatsonya.android.dto;

import com.citm.chatsonya.android.dto.model.timelinemodel;
import com.citm.chatsonya.android.dto.model.usersmodel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class timelineResponse {

    @SerializedName("profil")
    private ArrayList<usersmodel> ProfilResponse;
    @SerializedName("citm")
    private ArrayList<timelinemodel> citm;

    public ArrayList<usersmodel> getProfilResponse() {
        return ProfilResponse;
    }

    public void setProfilResponse(ArrayList<usersmodel> profilResponse) {
        ProfilResponse = profilResponse;
    }

    public ArrayList<timelinemodel> getCitm() {
        return citm;
    }

    public void setCitm(ArrayList<timelinemodel> citm) {
        this.citm = citm;
    }
}
