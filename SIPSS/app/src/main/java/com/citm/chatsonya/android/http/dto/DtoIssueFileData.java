

package com.citm.chatsonya.android.http.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class DtoIssueFileData implements Serializable {

    public String id;
    public String type;
    public String issue_id;
    public String caption;
    public String file;
    public DtoIssueFileData(){

    }

    public void setFile(String file) {
        this.file = file;
    }
}
