/*
 * Copyright 2016 Kejun Xia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.citm.chatsonya.android.http;

import com.citm.chatsonya.android.http.dto.DtoInspectionProject;
import com.citm.chatsonya.android.http.dto.DtoInspectionRiver;
import com.citm.chatsonya.android.http.dto.DtoInspectionSta;
import com.citm.chatsonya.android.http.dto.DtoInspectionStaPrasarana;
import com.citm.chatsonya.android.http.dto.DtoInspectionStaRiver;
import com.citm.chatsonya.android.http.dto.DtoIssue;
import com.citm.chatsonya.android.http.dto.DtoIssueChartByStatus;
import com.citm.chatsonya.android.http.dto.DtoIssueComment;
import com.citm.chatsonya.android.http.dto.DtoIssueFile;
import com.citm.chatsonya.android.http.dto.DtoNotification;
import com.citm.chatsonya.android.http.dto.DtoPost;
import com.citm.chatsonya.android.http.dto.DtoPostFile;
import com.citm.chatsonya.android.http.dto.DtoRefBalai;
import com.citm.chatsonya.android.http.dto.DtoRefIssueType;
import com.citm.chatsonya.android.http.dto.DtoRefLastUpdate;
import com.citm.chatsonya.android.http.dto.DtoUser;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface Api {
    @GET("user/validate_token")
    Call<DtoPost> UserValidateToken(
            @Query("token") String token);


    @FormUrlEncoded
    @POST("user/login/login")
    Call<DtoUser> UserLogin(@Field("data[username]") String username,
                            @Field("data[password]") String password);

    @FormUrlEncoded
    @POST("user/login/login_by_google")
    Call<DtoUser> UserLoginByGoogle(@Field("data[soc_id]") String soc_id);

    @FormUrlEncoded
    @POST("user/login/check_google_account")
    Call<DtoPost> CheckGoogleAccount(@Field("data[email]") String email);

    @FormUrlEncoded
    @POST("user/login/login_by_google")
    Call<DtoUser> LoginByGoogle(@Field("data[soc_id]") String soc_id);

    @FormUrlEncoded
    @POST("user/login/register_by_google")
    Call<DtoUser> RegisterByGoogle(@Field("data[soc_id]") String soc_id,
                                   @Field("data[email]") String email,
                                   @Field("data[firstname]") String firstname);

    @Multipart
    @POST("misc/issue_post/add")
    Call<DtoPost> MiscIssuePostAdd(@Query("token") String token,
                                   @Part("data[keterangan]") String keterangan,
                                   @Part("data[kategori]") String kategori,
                                   @Part MultipartBody.Part file);

    @Multipart
    @POST("misc/issue_post/edit")
    Call<DtoPost> MiscIssuePostEdit(@Query("token") String token,
                                    @Part("data[id]") String id,
                                    @Part("data[keterangan]") String keterangan,
                                    @Part("data[kategori]") String kategori,
                                    @Part MultipartBody.Part file);


    @Multipart
    @POST("user/login/update")
    Call<DtoUser> UpdateProfile(
            @Part("token") String token,
            @Part("data[firstname]") String firstname,
            @Part("data[lastname]") String lastname,
            @Part("data[email]") String email,
            @Part("data[alamat]") String alamat,
            @Part("data[phone]") String phone,
            @Part("data[photo]") String photo);


    @Multipart
    @POST("misc/file/upload")
    Call<DtoPostFile> MiscFileUpload(@Part("token") String token, @Part MultipartBody.Part file);

    @Multipart
    @POST("misc/file/upload_issue")
    Call<DtoPost> MiscFileUploadIssue(@Query("token") String token, @Query("ref_id") String ref_id, @Part MultipartBody.Part file);

    @Multipart
    @POST("issue/report/mine")
    Call<DtoIssue> IssueReportMine(
            @Part("token") String token,
            @Query("max_result") int max_result,
            @Query("sort") String sort,
            @Query("dir") String dir,
            @Query("q") String query,
            @Query("page") int page);

    @Multipart
    @POST("issue/report/all")
    Call<DtoIssue> IssueReportAll(
            @Part("token") String token,
            @Query("max_result") int max_result,
            @Query("sort") String sort,
            @Query("dir") String dir,
            @Query("q") String query,
            @Query("page") int page);

    @Multipart
    @POST("issue/report/delete")
    Call<DtoIssue> IssueReportDelete(
            @Part("token") String token,
            @Query("id") String id);


    @Multipart
    @POST("issue/map/all")
    Call<DtoIssue> IssueMapAll(
            @Part("token") String token,
            @Query("gps_lat") String gps_lat,
            @Query("gps_lng") String gps_lng,
            @Query("radius") String radius);

    @Multipart
    @POST("issue/chart/by_status")
    Call<DtoIssueChartByStatus> IssueChartByStatus(
            @Part("token") String token);


    @Multipart
    @POST("issue/report_post/add")
    Call<DtoIssue> IssueReportPostAdd(
            @Part("token") String token,
           // @Part("data[balai_id]") String balai_id,
            @Part("data[type_id]") String type_id,
            @Part("data[category]") String category,
            @Part("data[caption]") String caption,
            @Part("data[location]") String location,
            @Part("data[gps_lat]") String gps_lat,
            @Part("data[gps_lng]") String gps_lng,
            @Part("data[photo_1]") String photo_1,
            @Part("data[photo_2]") String photo_2,
            @Part("data[photo_3]") String photo_3,
            @Part("data[photo_4]") String photo_4);


    @Multipart
    @POST("issue/report_post/edit")
    Call<DtoIssue> IssueReportPostEdit(
            @Part("token") String token,
            @Part("data[id]") String id,
           // @Part("data[balai_id]") String balai_id,
            @Part("data[type_id]") String type_id,
            @Part("data[category]") String category,
            @Part("data[caption]") String caption,
            @Part("data[location]") String location);


    @Multipart
    @POST("issue/report_post/detail_by_id")
    Call<DtoIssue> IssueReportDetailById( @Part("token") String token,
                                          @Part("id") String id);

    @Multipart
    @POST("issue/report_file/set_favourite")
    Call<DtoIssueFile> IssueReportFileSetFavourite(@Part("token") String token,
                                                   @Part("id") String id);

    @Multipart
    @POST("issue/report_file/delete")
    Call<DtoIssueFile> IssueReportFileDelete(
            @Part("token") String token,
            @Query("id") String id);

    @Multipart
    @POST("issue/report_file/kerusakan")
    Call<DtoIssueFile> IssueReportFileKerusakan(
            @Part("token") String token,
            @Query("max_result") int max_result,
            @Query("sort") String sort,
            @Query("dir") String dir,
            @Query("q") String query,
            @Query("page") int page,
            @Part("issue_id") String issue_id);

    @Multipart
    @POST("issue/report_file/perbaikan")
    Call<DtoIssueFile> IssueReportFilePerbaikan(
            @Part("token") String token,
            @Query("max_result") int max_result,
            @Query("sort") String sort,
            @Query("dir") String dir,
            @Query("q") String query,
            @Query("page") int page,
            @Part("issue_id") String issue_id);

    @Multipart
    @POST("issue/report_file/finishing")
    Call<DtoIssueFile> IssueReportFileFinishing(
            @Part("token") String token,
            @Query("max_result") int max_result,
            @Query("sort") String sort,
            @Query("dir") String dir,
            @Query("q") String query,
            @Query("page") int page,
            @Part("issue_id") String issue_id);

    @Multipart
    @POST("issue/report_file_post/add")
    Call<DtoPost> IssueReportFileAdd(
            @Query("token") String token,
            @Part("data[type]") String type,
            @Part("data[issue_id]") String issue_id,
            @Part("data[caption]") String caption,
            @Part("data[file]") String file,
            @Part("data[filename]") String filename
    );
    @Multipart
    @POST("issue/report_file_post/edit")
    Call<DtoPost> IssueReportFilEdit(
            @Query("token") String token,
            @Part("data[id]") String id,
            @Part("data[type]") String type,
            @Part("data[issue_id]") String issue_id,
            @Part("data[caption]") String caption,
            @Part("data[file]") String file,
            @Part("data[filename]") String filename
    );


    @Multipart
    @POST("referensi/ref_balai/all")
    Call<DtoRefBalai> RefBalai(
            @Part("token") String token);


    @Multipart
    @POST("referensi/ref_issue_type/all")
    Call<DtoRefIssueType> RefIssueType(
            @Part("token") String token);
    @Multipart
    @POST("referensi/last_update/all")
    Call<DtoRefLastUpdate> RefLastUpdate(
            @Part("token") String token);

    @Multipart
    @POST("misc/notification/all")
    Call<DtoNotification> MiscNotificationAll(
            @Part("token") String token,
            @Query("max_result") int max_result,
            @Query("sort") String sort,
            @Query("dir") String dir,
            @Query("q") String query,
            @Query("page") int page);

    @Multipart
    @POST("misc/notification/delete")
    Call<DtoNotification> MiscNotificationDelete(
            @Part("token") String token,
            @Query("id") String id);


    @Multipart
    @POST("issue/report_comment/delete")
    Call<DtoIssueComment> IssueReportCommentDelete(
            @Part("token") String token,
            @Query("id") String id);

    @Multipart
    @POST("issue/report_comment/all")
    Call<DtoIssueComment> IssueReportCommentAll(
            @Part("token") String token,
            @Query("max_result") int max_result,
            @Query("sort") String sort,
            @Query("dir") String dir,
            @Query("q") String query,
            @Query("page") int page,
            @Part("issue_id") String issue_id);

    @Multipart
    @POST("issue/report_comment_post/add")
    Call<DtoPost> IssueReportCommentAdd(
            @Query("token") String token,
            @Part("data[issue_id]") String issue_id,
            @Part("data[caption]") String caption
    );
    @Multipart
    @POST("issue/report_comment_post/edit")
    Call<DtoPost> IssueReportCommentEdit(
            @Query("token") String token,
            @Part("data[id]") String id,
            @Part("data[issue_id]") String issue_id,
            @Part("data[caption]") String caption
    );

    @Multipart
    @POST("misc/notification/unread")
    Call<DtoNotification> MiscNotificationUnread(
            @Part("token") String token);

    @Multipart
    @POST("inspection/project/all")
    Call<DtoInspectionProject> InspectionProjectAll(
            @Part("token") String token,
            @Query("max_result") int max_result,
            @Query("sort") String sort,
            @Query("dir") String dir,
            @Query("q") String query,
            @Query("page") int page);

    @Multipart
    @POST("inspection/project/delete")
    Call<DtoInspectionProject>  InspectionProjectDelete(
            @Part("token") String token,
            @Query("id") String id);
    @Multipart
    @POST("inspection/project_post/add")
    Call<DtoPost> InspectionProjectAdd(
            @Query("token") String token,
            @Part("data[name]") String name,
            @Part("data[description]") String description
    );
    @Multipart
    @POST("inspection/project_post/edit")
    Call<DtoPost> InspectionProjectEdit(
            @Query("token") String token,
            @Part("data[id]") String id,
            @Part("data[name]") String name,
            @Part("data[description]") String description
    );

    @Multipart
    @POST("inspection/river/all")
    Call<DtoInspectionRiver> InspectionRiverAll(
            @Part("token") String token,
            @Query("inspection_project_id") String inspection_project_id,
            @Query("max_result") int max_result,
            @Query("sort") String sort,
            @Query("dir") String dir,
            @Query("q") String query,
            @Query("page") int page);

    @Multipart
    @POST("inspection/river/delete")
    Call<DtoInspectionRiver>  InspectionRiverDelete(
            @Part("token") String token,
            @Query("id") String id);
    @Multipart
    @POST("inspection/river_post/add")
    Call<DtoPost> InspectionRiverAdd(
            @Query("token") String token,
            @Part("data[inspection_project_id]") String inspection_project_id,
            @Part("data[name]") String name,
            @Part("data[description]") String description
    );
    @Multipart
    @POST("inspection/river_post/edit")
    Call<DtoPost> InspectionRiverEdit(
            @Query("token") String token,
            @Part("data[id]") String id,
            @Part("data[inspection_project_id]") String inspection_project_id,
            @Part("data[name]") String name,
            @Part("data[description]") String description
    );


    @Multipart
    @POST("inspection/sta/all")
    Call<DtoInspectionSta> InspectionStaAll(
            @Part("token") String token,
            @Query("inspection_river_id") String inspection_river_id,
            @Query("max_result") int max_result,
            @Query("sort") String sort,
            @Query("dir") String dir,
            @Query("q") String query,
            @Query("page") int page);
    @Multipart
    @POST("inspection/sta/gps")
    Call<DtoInspectionSta> InspectionStaMap(
            @Part("token") String token,
            @Query("inspection_river_id") String inspection_river_id);

    @Multipart
    @POST("inspection/sta/delete")
    Call<DtoInspectionSta>  InspectionStaDelete(
            @Part("token") String token,
            @Query("id") String id);

    @Multipart
    @POST("inspection/sta_post/add")
    Call<DtoPost> InspectionStaAdd(
            @Query("token") String token,
            @Part("data[inspection_river_id]") String inspection_river_id,
            @Part("data[name]") String name,
            @Part("data[description]") String description,
            @Part("data[gps_lat]") String gps_lat,
            @Part("data[gps_lng]") String gps_lng,
            @Part("data[kota]") String kota,
            @Part("data[kecamatan]") String kecamatan,
            @Part("data[desa]") String desa,
            @Part("data[dusun]") String dusun
    );
    @Multipart
    @POST("inspection/sta_post/edit")
    Call<DtoPost> InspectionStaEdit(
            @Query("token") String token,
            @Part("data[id]") String id,
            @Part("data[inspection_river_id]") String inspection_river_id,
            @Part("data[name]") String name,
            @Part("data[description]") String description,
            @Part("data[gps_lat]") String gps_lat,
            @Part("data[gps_lng]") String gps_lng,
            @Part("data[kota]") String kota,
            @Part("data[kecamatan]") String kecamatan,
            @Part("data[desa]") String desa,
            @Part("data[dusun]") String dusun
    );
    @Multipart
    @POST("inspection/sta_foto_post/edit")
    Call<DtoPost> InspectionStaFotoEdit(
            @Query("token") String token,
            @Part("data[id]") String id,
            @Part MultipartBody.Part photo_kiri,
            @Part MultipartBody.Part photo_tengah,
            @Part MultipartBody.Part photo_kanan
    );
    @Multipart
    @POST("inspection/sta_river_post/add_edit")
    Call<DtoPost> InspectionStaRiverAdd(
            @Query("token") String token,
            @Part("data[inspection_sta_id]") String inspection_sta_id,
            @Part("data[data_json]") String data_json,
            @Part MultipartBody.Part sketsa
    );
    @Multipart
    @POST("inspection/sta_river_post/get_detail_by_inspection_sta_id")
    Call<DtoInspectionStaRiver>  InspectionStaRiverDataDetail(
            @Part("token") String token,
            @Query("inspection_sta_id") String inspection_sta_id);

    @Multipart
    @POST("inspection/sta_prasarana_post/add_edit")
    Call<DtoPost> InspectionStaPrasaranaAdd(
            @Query("token") String token,
            @Part("data[inspection_sta_id]") String inspection_sta_id,
            @Part("data[data_json]") String data_json
    );
    @Multipart
    @POST("inspection/sta_prasarana_post/add_edit_kiri")
    Call<DtoPost> InspectionStaPrasaranaKiriAdd(
            @Query("token") String token,
            @Part("data[inspection_sta_id]") String inspection_sta_id,
            @Part("data[data_json]") String data_json
    );
    @Multipart
    @POST("inspection/sta_prasarana_post/add_edit_kanan")
    Call<DtoPost> InspectionStaPrasaranaKananAdd(
            @Query("token") String token,
            @Part("data[inspection_sta_id]") String inspection_sta_id,
            @Part("data[data_json]") String data_json
    );
    @Multipart
    @POST("inspection/sta_prasarana_post/get_detail_by_inspection_sta_id")
    Call<DtoInspectionStaPrasarana>  InspectionStaPrasaranaDataDetail(
            @Part("token") String token,
            @Query("inspection_sta_id") String inspection_sta_id);


}
