package com.citm.chatsonya.android.dto.model;

public class TabModel {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
