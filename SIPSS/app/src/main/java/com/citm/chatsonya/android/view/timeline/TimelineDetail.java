package com.citm.chatsonya.android.view.timeline;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.controller.timeline.TimelineDetailController;
import com.citm.chatsonya.android.dto.PostResponse;
import com.citm.chatsonya.android.dto.model.Message;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.citm.chatsonya.android.http.AllDataService;
import com.shipdream.lib.android.mvc.MvcFragment;
import com.shipdream.lib.android.mvc.NavigationManager;
import com.shipdream.lib.android.mvc.Reason;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class TimelineDetail extends MvcFragment<TimelineDetailController> implements View.OnClickListener {
    ImageView imageView;
    TextView textView;
    Toolbar mToolbar;
    EditText balasStatus;
    ImageButton statusimagedetailtimeline;
    DatabaseHandler dbHandler;
    ArrayList<Message> messagesList;
    String id_conversation;
    SharedPreferences mSettings;
    SimpleDateFormat crTime;
    Calendar c;
    @Inject
    NavigationManager navigationManager;
    AllDataService service;
    @Override
    protected Class<TimelineDetailController> getControllerClass() {
        return TimelineDetailController.class;
    }
    @Override
    protected int getLayoutResId() {
        return R.layout.detailtimeline;
    }
    @Override
    protected void onViewReady(View view, Bundle savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);
        imageView = (ImageView)view.findViewById(R.id.imageViewTimelinedetail);
        textView = (TextView)view.findViewById(R.id.textViewtimelinedetail);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        balasStatus = (EditText)view.findViewById(R.id.balasstatus);
        dbHandler = new DatabaseHandler(getContext());
        statusimagedetailtimeline = (ImageButton)view.findViewById(R.id.statusimagedetailtimeline);
        service = RetrofitInstance.RetrofitInstance().create(AllDataService.class);
        mSettings = getActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE);
        if (mToolbar != null) {
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        }
        if (getActivity() instanceof AppCompatActivity) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setHasOptionsMenu(true);
        mToolbar.setTitle(controller.getModel().getName());
        statusimagedetailtimeline.setOnClickListener(this);

    }
    @Override
    public void update() {
        textView.setText(controller.getModel().getText_timeline());
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigationManager.navigate(getActivity()).back();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.statusimagedetailtimeline:
                // os adalah id status
//                TastyToast.makeText(getContext(),balasStatus.getText().toString()+"iduser"+controller.getModel().getUsers_id()+"id post"+controller.getModel().getId(), TastyToast.LENGTH_LONG, TastyToast.WARNING);
                messagesList = new ArrayList<>();
                try{
//                    messagesList = dbHandler.getprivatechat(mSettings.getString("id_user" ,null) , controller.getModel().getUsers_id());
                    //membuat kode unik untuk id_CONVERSASION dengan rumus id user sendiri dan user penerima
                    int[] array ={ Integer.parseInt(mSettings.getString("id_user" ,null)),Integer.parseInt(controller.getModel().getUsers_id())};
                    Arrays.sort(array);
                    if(messagesList.size() == 0 ){
                        String nh = String.valueOf(array[0])+String.valueOf(array[1]);
                        id_conversation = nh;
                    }else{
                        id_conversation=messagesList.get(0).getId_conversation();
                    }
                    c = Calendar.getInstance();
                    crTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String strCreation = crTime.format(c.getTime());
                    Message msg;
                    String message = balasStatus.getText().toString();
                    String reciver = controller.getModel().getUsers_id();
                    String users = controller.getModel().getName();
                    String sender = mSettings.getString("id_user" ,null);
                    msg = new Message("-1",  reciver, sender,message, id_conversation, strCreation ,users);
                    msg.setMessageType(0);
                    msg.setStatus(1);
                    msg.setId_conversation(id_conversation);
                    messagesList.add(msg);
                    long id = dbHandler.AddMessage(msg);
                    if (id != -1) {
                        Call<PostResponse> call = service.pesan("Bearer " + mSettings.getString("key", null), balasStatus.getText().toString(),mSettings.getString("user","missing"),reciver,sender, id_conversation , "-1");
                        call.enqueue(new Callback<PostResponse>() {
                            @Override
                            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {

                                if (response.isSuccessful()) {
                                    Log.e(TAG, "onResponse: " + response.body().toString());
                                } else {
                                    Log.e(TAG, "onResponse: " + response.errorBody().source());
                                }

                            }

                            @Override
                            public void onFailure(Call<PostResponse> call, Throwable t) {
                                Log.e(TAG, "onFailure: " + t);

                            }
                        });
                    }
                    navigationManager.navigate(getContext()).back();
                    Log.e("Lagi Lagi", "onCreate:id_conversation "+id_conversation );
                }catch (Exception e){
                    Log.e("lagi Lagi", "onCreate: "+e );
                }
                break;


        }
    }
}
