package com.citm.chatsonya.android.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.dto.model.timelinemodel;
import com.citm.chatsonya.android.view.fragment.Timeline;
import com.shipdream.lib.android.mvc.NavigationManager;

import java.util.List;

import javax.inject.Inject;

public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.ViewHolder> {
    private List<timelinemodel> datalist;
    Timeline context;
    @Inject
    NavigationManager navigationManager;

    public TimelineAdapter(List<timelinemodel> datalist, Timeline context , NavigationManager navigationManager) {
        this.datalist = datalist;
        this.context = context;
        this.navigationManager = navigationManager;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_timeline,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.nama.setText(datalist.get(position).getName());
        holder.waktudata.setText(datalist.get(position).getCreated_at());
//        TextDraweble drawebl = TextDrawable.builder();

//        TextDrawable drawable = TextDrawable.builder()
//                .beginConfig()
//                .textColor(Color.WHITE)
//                .useFont(Typeface.DEFAULT)
//                .fontSize(18) /* size in px */
//                .bold()
//                .toUpperCase()
//                .endConfig()
//                .buildRoundRect(datalist.get(position).getText_timeline(), ColorGenerator.MATERIAL.getRandomColor() , 100);
//        holder.circleImageView.setImageDrawable(drawable);
//        holder.listtimeline.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                navigationManager.navigate(context).to(TimelineDetailController.class);
//                navigationManager.navigate(context).with(TimelineDetailController.class, new Preparer<TimelineDetailController>() {
//                    @Override
//                    public void prepare(TimelineDetailController controller_to) {
//                        Gson gs = new Gson();
//                        String s = gs.toJson(datalist.get(position));
//                        controller_to.daritimelineadapter(s);
//                    }
//                }).to(TimelineDetailController.class);
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nama , waktudata;
        ImageView circleImageView;

        LinearLayout listtimeline;
        public ViewHolder(View itemView) {
            super(itemView);
            nama = (TextView)itemView.findViewById(R.id.namauserstatus);
            waktudata =(TextView)itemView.findViewById(R.id.waktudate);
            circleImageView = (ImageView) itemView.findViewById(R.id.imageviewtimeline);
            listtimeline = (LinearLayout)itemView.findViewById(R.id.listtimeline);



        }
    }


}
