package com.citm.chatsonya.android.view.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.citm.chatsonya.android.Adapter.BeritaBaruAdapter;
import com.citm.chatsonya.android.BaseTabFragment;
import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.controller.fragment.BeritaBaruController;
import com.citm.chatsonya.android.dto.BeritaBaruModelResponse;
import com.citm.chatsonya.android.dto.model.BeritaBaruModel;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.helpers.AsyncView;
import com.citm.chatsonya.android.helpers.LifeCycleMonitor;
import com.citm.chatsonya.android.helpers.LifeCycleMonitorBeritaBaru;
import com.citm.chatsonya.android.http.AllDataService;
import com.shipdream.lib.android.mvc.NavigationManager;
import com.shipdream.lib.android.mvc.Reason;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class BeritaBaru extends BaseTabFragment<BeritaBaruController> implements AsyncView {
    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    Context context;
    ArrayList<BeritaBaruModel> datalist = new ArrayList<>();
    BeritaBaruAdapter adapter;


    @Inject
    private LifeCycleMonitorBeritaBaru lifeCycleMonitorBeritaBaru;
    @Inject
    private NavigationManager navigationManager;
    @Override
    protected LifeCycleMonitor getLifeCycleMonitor() {
        return lifeCycleMonitorBeritaBaru;
    }

    @Override
    protected Class<BeritaBaruController> getControllerClass() {
        return BeritaBaruController.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_tab_beritabaru;
    }

    @Override
    public void onViewReady(View view, Bundle savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view_beritabaru);
        recyclerView.setHasFixedSize(true);
        setHasOptionsMenu(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.beritabaru_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void update() {
        SharedPreferences mSettings = getActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE);
        Log.e(TAG, "update: "+ mSettings.getString("key","missing"));
        AllDataService service = RetrofitInstance.RetrofitInstance().create(AllDataService.class);
        Call<BeritaBaruModelResponse> call = service.berita("Bearer "+mSettings.getString("key","missing"));
        call.enqueue(new Callback<BeritaBaruModelResponse>() {
            @Override
            public void onResponse(Call<BeritaBaruModelResponse> call, Response<BeritaBaruModelResponse> response) {
                if(response.isSuccessful()){
                    List<BeritaBaruModel> BR = response.body().getResult();
                    adapter = new BeritaBaruAdapter(BR , getActivity() , navigationManager);
                    recyclerView.setAdapter(adapter);
                }else{
                    Log.e(TAG, "onResponse error: "+response.errorBody().toString() );
                }

            }

            @Override
            public void onFailure(Call<BeritaBaruModelResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t );



            }
        });
    }

    @Override
    public void showLoadingStatus() {

        Log.e(TAG, "showLoadingStatus: " );
//        navigationManager.navigate(this).to(splashcontroller.class ,new Forwarder().clearAll());

    }

    @Override
    public void hideLoadingStatus() {

    }

    @Override
    public void loginsubmit(String status, String Key, String iduser) {

    }
}

