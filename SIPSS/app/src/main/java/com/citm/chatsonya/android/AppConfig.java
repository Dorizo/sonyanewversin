package com.citm.chatsonya.android;

public class AppConfig {
    /**
     * Debug Mode
     * true : API_URL_DEV, false : API_URL
     */
    public static final boolean DEBUG = false;
    /**
     * Internal Log Mode
     * true : force logging internal
     * false : DEBUG = true, logging to Fabric
     */
    static final boolean INTERNAL_LOG_MODE = true;
    /**
     * URL for Development
     */
    public static final String API_URL_DEV = "http://192.160.56.1/sipss";
    /**
     * URL for Production
     */
    public static final String API_URL = "http://sipss.com";
    /**
     * Max Result Per-page
     */
    public static final int MAX_RESULTS = 40;
    /**
     * API version
     * Example : {API_URL}/{API_VERSION}/parameters
     */
    public static final int API_VERSION = 1;
    /**
     * Enable Leak Canary
     * for Memory Leak Detection
     */
    static final boolean ENABLE_LEAK_CANARY = false;
    /**
     * Font Path
     */
    static final String DEFAULT_FONT_PATH = "fonts/Roboto-Regular.ttf";


    public static final String GOOGLE_MAP_API_KEY = "AIzaSyBFj0-QPpxLQbgGh-lTQRQYwO9HTwggEXs";
}
