package com.citm.chatsonya.android;


import android.app.Application;
import android.content.Context;

import com.shipdream.lib.android.mvc.Mvc;
import com.shipdream.lib.poke.Provides;
import com.shipdream.lib.poke.exception.ProvideException;
import com.shipdream.lib.poke.exception.ProviderConflictException;


public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        try {
            Mvc.graph().getRootComponent().register(new Object(){
                @Provides
                @AppContext
                public Context context() {
                    return getApplicationContext();
                }
            });
        } catch (ProvideException e) {
            e.printStackTrace();
        } catch (ProviderConflictException e) {
            e.printStackTrace();
        }
    }

}
