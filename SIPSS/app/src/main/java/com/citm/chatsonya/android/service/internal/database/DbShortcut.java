package com.citm.chatsonya.android.service.internal.database;

import com.orm.SugarRecord;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class DbShortcut extends SugarRecord {
    String _id;
    String title;
    String class_name;
    String icon;
    String sort_order;
    public String show;


    public DbShortcut() {

    }

    public DbShortcut(String id, String title, String class_name, String icon, String sort_order, String show) {
        this._id = id;
        this.title = title;
        this.class_name = class_name;
        this.icon = icon;
        this.sort_order = sort_order;
        this.show = show;
    }

}

