package com.citm.chatsonya.android.dto.model;

public class usersmodel {
    String id, name,email,telp,image,created_at,updated_at;

    private boolean isSelected = false;

    public usersmodel(String id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public usersmodel(String id, String name, String email, String telp, String image, String created_at, String updated_at) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.telp = telp;
        this.image = image;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
