package com.citm.chatsonya.android.view.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.citm.chatsonya.android.Adapter.TimelineAdapter;
import com.citm.chatsonya.android.BaseTabFragment;
import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.controller.fragment.TimelineController;
import com.citm.chatsonya.android.controller.timeline.createTexttimelineController;
import com.citm.chatsonya.android.dto.model.ViewuitimelineModel;
import com.citm.chatsonya.android.helpers.LifeCycleMonitor;
import com.citm.chatsonya.android.helpers.LifeCycleMonitorTimeline;
import com.shipdream.lib.android.mvc.NavigationManager;
import com.shipdream.lib.android.mvc.Reason;

import javax.inject.Inject;

public class Timeline extends BaseTabFragment<TimelineController> implements ViewuitimelineModel {
    private RecyclerView recyclerView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayoutManager mLayoutManager;
    FloatingActionButton statustext;
    SharedPreferences mSettings;
    TimelineAdapter timelineAdapter;
    Context context;
    @Inject
    LifeCycleMonitorTimeline lifeCycleMonitorTimeline;
    @Inject
    private NavigationManager navigationManager;

    @Override
    protected LifeCycleMonitor getLifeCycleMonitor() {
        return lifeCycleMonitorTimeline;
    }

    @Override
    protected Class<TimelineController> getControllerClass() {
        return TimelineController.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.timelinelayout;
    }
    @Override
    public void onViewReady(View view, Bundle savedInstanceState, final Reason reason) {
        mSettings = getActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE);
        super.onViewReady(view, savedInstanceState, reason);
        recyclerView = (RecyclerView)view.findViewById(R.id.timelineRecycleview);
        mSwipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefreshLayouttimeline);
        recyclerView.setHasFixedSize(true);
        controller.viewtimeline(mSettings.getString("key","missing"),null);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        refreshdata();
        statustext = (FloatingActionButton)view.findViewById(R.id.statustext);
        statustext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigationManager.navigate(context).to(createTexttimelineController.class);
            }
        });

    }

    private void refreshdata() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               update();
            }
        });
    }

    @Override
    public void update() {

        if (controller.getModel().getTimelinemodels() !=null) {
            timelineAdapter = new TimelineAdapter(controller.getModel().getTimelinemodels(), this , navigationManager);
            recyclerView.setAdapter(timelineAdapter);
            Log.e("rinciantimeline", "update: "+controller.getModel().getTimelinemodels() );
//            TastyToast.makeText(getActivity() , "data ADA" , TastyToast.LENGTH_LONG,TastyToast.WARNING);
            mSwipeRefreshLayout.setRefreshing(false);
        }else{

            mSwipeRefreshLayout.setRefreshing(true);
        }
    }

    @Override
    public void timelinesubmit() {

    }
}
