

package com.citm.chatsonya.android.http.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class DtoIssueCommentData implements Serializable {

    public String id;
    public String issue_id;
    public String user_id;
    public String fullname;
    public String photo;
    public String caption;
    public String file;
    public String important;
    public String user_added;
    public String user_modified;
    public String date_added;
    public String date_modified;
    public DtoIssueCommentData(){

    }

}
