package com.citm.chatsonya.android.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.dto.model.Message;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

import static net.alhazmy13.mediapicker.Image.ImageTags.Tags.TAG;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    Context mContext;
    ArrayList<Message> messagesList;
    String SELF, fontSize, today;
    Boolean showBubble, showStamp;

    public ChatAdapter(Context context, ArrayList<Message> messagesList, String self, boolean showBubble, boolean showStamp, String fontSize) {
        this.mContext = context;
        this.messagesList = messagesList;
        this.SELF = self;
        this.showBubble = showBubble;
        this.showStamp = showStamp;
        this.fontSize = fontSize;
        Calendar calendar = Calendar.getInstance();
        this.today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == 5) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatmodel_right, parent, false);
        }
        else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatmodel_left, parent, false);
        }

        return new ViewHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        Message msg = messagesList.get(position);
        if (msg.getSender().equals(SELF)) {
            return 5;
        }else{
            return 0;
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
    try {
        holder.message.setText(messagesList.get(position).getMessage());
    }catch (Exception e){
        Log.e(TAG, "onBindViewHolder: "+e );
    }
    }

    public String getTimeStamp(String stamp, boolean isReceived) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (isReceived) { format.setTimeZone(TimeZone.getTimeZone("UTC")); }
        String timestamp = "";

        today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(stamp);
            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            timestamp = format.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;
    }

    @Override
    public int getItemCount() {
        return messagesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView message, timestamp, sender;
        public CircleImageView icon;
        public ImageView status, imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            message = (TextView) itemView.findViewById(R.id.txtMessage);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);
            icon = (CircleImageView) itemView.findViewById(R.id.icon);
            sender = (TextView) itemView.findViewById(R.id.sender);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
        }
    }
}
