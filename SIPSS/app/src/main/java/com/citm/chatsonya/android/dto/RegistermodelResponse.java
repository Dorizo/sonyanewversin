package com.citm.chatsonya.android.dto;

import com.citm.chatsonya.android.dto.model.Registermodel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RegistermodelResponse {
    @SerializedName("citm")
    private ArrayList<Registermodel> result;

    public ArrayList<Registermodel> getResult() {
        return result;
    }

    public void setResult(ArrayList<Registermodel> result) {
        this.result = result;
    }
}
