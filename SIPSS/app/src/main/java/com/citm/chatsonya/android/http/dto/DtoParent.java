package com.citm.chatsonya.android.http.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;


public class DtoParent implements Serializable {
    @SerializedName("error")
    private String error;
    @SerializedName("granted")
    private String granted;
    @SerializedName("message")
    private String message;
    @SerializedName("filter_sort_by")
    private HashMap<String,String> filter_sort_by;
    @SerializedName("pagination_current")
    private int pagination_current;
    @SerializedName("pagination_total")
    private int pagination_total;
    @SerializedName("pagination_total_data")
    private int pagination_total_data;
    @SerializedName("pagination_max_result")
    private int pagination_max_result;

    public int getPagination_current() {
        return pagination_current;
    }

    public int getPagination_total() {
        return pagination_total;
    }
    public int getPagination_total_data() {
        return pagination_total_data;
    }
    public int getPagination_max_result() {
        return pagination_max_result;
    }

    public HashMap<String, String> getFilter_sort_by() {
        return filter_sort_by;
    }
    public boolean isError(){
        return Boolean.parseBoolean(error);
    }
    public boolean isGranted(){
        return Boolean.parseBoolean(granted);
    }
    public String getMessage(){
        return message;
    }
}
